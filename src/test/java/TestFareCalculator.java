//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.boot.context.embedded.LocalServerPort;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.web.client.TestRestTemplate;
//import org.springframework.http.ResponseEntity;
//import org.springframework.test.context.TestPropertySource;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import com.afkl.cases.df.WebConfiguration;
//
//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = WebConfiguration.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//@TestPropertySource(properties = {"management.port=0"})
//public class TestFareCalculator {
//
//	@LocalServerPort
//	private int port;
//
//	@Value("${local.management.port}")
//	private int mgt;
//
//	@Autowired
//	private TestRestTemplate testRestTemplate;
//
//	@Test
//	public void testFares() throws Exception {
//		@SuppressWarnings("rawtypes")
//		ResponseEntity<String> entity = this.testRestTemplate.getForEntity(
//				"http://localhost:"+port+"/travel/fares?source=BUD&dest=AGP", String.class);
//		System.out.println(entity);
//	}
//}