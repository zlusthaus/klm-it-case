//
//import static java.lang.String.format;
//import static java.util.Arrays.asList;
//
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
//import org.springframework.security.oauth2.client.OAuth2RestTemplate;
//import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
//import org.springframework.test.context.TestPropertySource;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import com.afkl.cases.df.WebConfiguration;
//
//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = WebConfiguration.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//@TestPropertySource(properties = {"management.port=0"})
//public class TestActualEndpoint {
//
//	@Value("${klm.travelapi.url:http://localhost:8080}")
//	private String baseUrl;
//	@Value("${klm.travelapi.authorizeUrl:http://localhost:8080/oauth/token}")
//	private String authorizeUrl;
//	@Value("${klm.travelapi.tokenUrl:http://localhost:8080/oauth/token}")
//	private String tokenUrl;
//
//	@Value("${klm.travelapi.clientId:travel-api-client}")
//	private String clientId;
//
//	@Value("${klm.travelapi.secret:psw}")
//	private String secret;
//	
//	
//	@Test
//    public void clientAccess() {
//		ClientCredentialsResourceDetails resourceDetails = new ClientCredentialsResourceDetails();
//        resourceDetails.setAccessTokenUri(tokenUrl);
//        resourceDetails.setClientId(clientId);
//        resourceDetails.setClientSecret(secret);
//        resourceDetails.setGrantType("client_credentials");
//        resourceDetails.setScope(asList("read", "write"));        
//        DefaultOAuth2ClientContext clientContext = new DefaultOAuth2ClientContext();
//
//        OAuth2RestTemplate restTemplate = new OAuth2RestTemplate(resourceDetails, clientContext);
////        restTemplate.setMessageConverters(asList(new MappingJackson2HttpMessageConverter()));
//
//        final String greeting = restTemplate.getForObject(format("http://localhost:8080/airports?term=udap"), String.class);
//
//        System.out.println(greeting);
//    }
//}