package com.afkl.cases.df.downstreamdomain;

import com.fasterxml.jackson.databind.jsonschema.JsonSerializableSchema;

@JsonSerializableSchema
public class AirportInformation {

	public String code;
	public String name;	
	public String description;
	
	
	//we don't need these right now	
//	public Coordinates coordinates;
//
//	public AirportInformation parent;
	
}
