package com.afkl.cases.df.downstreamdomain;

import java.math.BigDecimal;

import com.fasterxml.jackson.databind.jsonschema.JsonSerializableSchema;

@JsonSerializableSchema
public class Fare{

	public BigDecimal amount;
	public Currency currency;
	public String origin, destination;

}
