package com.afkl.cases.df.downstreamdomain;

import java.util.List;

import com.fasterxml.jackson.databind.jsonschema.JsonSerializableSchema;

@JsonSerializableSchema
public class AirportInternalResponse {
	
	public List<AirportInformation> locations;
	
}
