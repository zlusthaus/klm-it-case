package com.afkl.cases.df.downstreamdomain;

import com.fasterxml.jackson.databind.jsonschema.JsonSerializableSchema;

@JsonSerializableSchema
public class AirportResponse {

	public AirportInternalResponse _embedded;
	
}
