package com.afkl.cases.df;

import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

import org.slf4j.MDC;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * A SLF4J MDC-compatible {@link ThreadPoolExecutor}.
 * <p>
 * In general, MDC is used to store diagnostic information (e.g. logfile name) in per-thread variables, to facilitate logging. However, although MDC
 * data is passed to thread children, this doesn't work when threads are reused in a thread pool. This is a drop-in replacement for
 * {@link ThreadPoolTaskExecutor} sets MDC data before each task appropriately.
 * </p>
 *
 * @author Dennis Schulte
 * 
 * Added some methods so that we can use it here as well.
 * 
 * @author zoltanlusthaus
 */
public class MdcThreadPoolTaskExecutor extends ThreadPoolTaskExecutor {

	private static final long serialVersionUID = 1L;
	private boolean useFixedContext = false;
	private Map<String, String> fixedContext;

	public MdcThreadPoolTaskExecutor() {
		super();
	}

	public MdcThreadPoolTaskExecutor(Map<String, String> fixedContext) {
		super();
		this.fixedContext = fixedContext;
		useFixedContext = (fixedContext != null);
	}

	private Map<String, String> getContextForTask() {
		return useFixedContext ? fixedContext : MDC.getCopyOfContextMap();
	}

	/**
	 * All executions will have MDC injected. {@code ThreadPoolExecutor}'s submission methods ({@code submit()} etc.)
	 * all delegate to this.
	 */
	@Override
	public void execute(Runnable command) {
		super.execute(wrapRunnable(command, getContextForTask()));
	}

	@Override
	public Future<?> submit(Runnable command) {
		return super.submit(wrapRunnable(command, getContextForTask()));
	}

	@Override
	public void execute(Runnable command, long ti) {
		super.execute(wrapRunnable(command, getContextForTask()), ti);
	}
	
	public <T> Future<T> submit(Callable<T> callable) {		
		return super.submit(wrapCallable(callable, getContextForTask()));
	}
	
	//TODO and we could put similars for submitting listenables..

	public static <T> Callable<T> wrapCallable(final Callable<T> callable, final Map<String, String> context) {
		return new Callable<T>() {
			@Override
			public T call() throws Exception {
				Map<String, String> previous = MDC.getCopyOfContextMap();
				if (context == null) {
					MDC.clear();
				} else {
					MDC.setContextMap(context);
				}
				try {
					return callable.call();
				} finally {
					if (previous == null) {
						MDC.clear();
					} else {
						MDC.setContextMap(previous);
					}
				}
			}
		};
	}
	
	public static Runnable wrapRunnable(final Runnable runnable, final Map<String, String> context) {
		return new Runnable() {
			@Override
			public void run() {
				Map<String, String> previous = MDC.getCopyOfContextMap();
				if (context == null) {
					MDC.clear();
				} else {
					MDC.setContextMap(context);
				}
				try {
					runnable.run();
				} finally {
					if (previous == null) {
						MDC.clear();
					} else {
						MDC.setContextMap(previous);
					}
				}
			}
		};
	}
}