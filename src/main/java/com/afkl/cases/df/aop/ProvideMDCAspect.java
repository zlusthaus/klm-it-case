package com.afkl.cases.df.aop;

import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.MDC;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

/**
 * This aspect will give a unique ID (RID) before the annotated joinpoint
 * 
 * @author zoltanlusthaus
 *
 */
@Aspect
public class ProvideMDCAspect {

	public static final AtomicLong counter = new AtomicLong();
	private static final String REQUEST_ID_LOG_KEY = "RID";
	
	@Before("@annotation(ProvideMDC) && execution(* *(..))")
	public void mdcBefore(JoinPoint joinPoint) throws Throwable {
		if (MDC.get(REQUEST_ID_LOG_KEY) == null)
			MDC.put(REQUEST_ID_LOG_KEY, " {Rq #"+counter.incrementAndGet()+"}");
	}

	@After("@annotation(ProvideMDC) && execution(* *(..))")
	public void mdcAfter(JoinPoint joinPoint) throws Throwable {
		MDC.remove(REQUEST_ID_LOG_KEY);					
	}

}