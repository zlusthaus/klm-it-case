package com.afkl.cases.df.aop;

/**
 * To annotate functions that need to be tracked, so MDC should be used - see by @ProvideMDCAspect
 * 
 * 
 * @author zoltanlusthaus
 *
 */
public @interface ProvideMDC {

}
