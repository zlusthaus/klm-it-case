package com.afkl.cases.df.aop;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.perf4j.slf4j.Slf4JStopWatch;

/**
 * This aspect will measure the total, minimum and maximum time elapsed in
 * milliseconds of each function annotated with @ProvideMetrics.
 * 
 * @author zoltanlusthaus
 *
 */
@Aspect
public class ProvideMetricsAspect {

	public static final Map<String, AtomicLong> measurements = new HashMap<>();

	public static final Object mutex = new Object();

	@Around("@annotation(ProvideMetrics) && execution(* *(..))")
	public Object meterAround(ProceedingJoinPoint joinPoint) throws Throwable {
		String logName = "slf4j.timer." + joinPoint.getSignature().getName();
		ensureMetricsPresentFor(logName);

		Slf4JStopWatch sw = new Slf4JStopWatch(logName);
		
		try {
			Object o = joinPoint.proceed();
			return o;
		} finally {
			sw.stop();
			long t = sw.getElapsedTime();

			// measuring the maximum, minimum and total
			measurements.get(logName + ".total").getAndUpdate(current -> current + t);
			measurements.get(logName + ".min").getAndUpdate(current -> Math.min(t, current));
			measurements.get(logName + ".max").getAndUpdate(current -> Math.max(t, current));
		}
	}

	private static void ensureMetricsPresentFor(String logName) {
		if (!measurements.containsKey(logName)) {
			synchronized (mutex) {//we could synch for logName, right?
				measurements.putIfAbsent(logName + ".total", new AtomicLong(0));
				measurements.putIfAbsent(logName + ".min", new AtomicLong(Long.MAX_VALUE));
				measurements.putIfAbsent(logName + ".max", new AtomicLong(0));
			}
		}
	}

}