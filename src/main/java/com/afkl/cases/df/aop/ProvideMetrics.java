package com.afkl.cases.df.aop;

/**
 * To annotate functions that should be included in metrics measurement by @ProvideMetricsAspect
 * 
 * 
 * @author zoltanlusthaus
 *
 */
public @interface ProvideMetrics {

}
