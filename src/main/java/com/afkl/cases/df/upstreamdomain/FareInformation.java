package com.afkl.cases.df.upstreamdomain;

import com.afkl.cases.df.downstreamdomain.AirportInformation;
import com.fasterxml.jackson.databind.jsonschema.JsonSerializableSchema;

@JsonSerializableSchema
public class FareInformation {

	public AirportInformation source;
	public AirportInformation destination;
	public Money cost;
	
	boolean successful;
	
	public FareInformation(){
		
	}
	
	public FareInformation(AirportInformation source, AirportInformation destination, Money cost){
		this.source = source;
		this.destination = destination;
		this.cost = cost;
		successful = cost != null && source != null && destination != null;
	}

}
