package com.afkl.cases.df.upstreamdomain;

import java.math.BigDecimal;

import com.afkl.cases.df.downstreamdomain.Currency;
import com.afkl.cases.df.downstreamdomain.Fare;
import com.fasterxml.jackson.databind.jsonschema.JsonSerializableSchema;

@JsonSerializableSchema
public class Money {
	
	public BigDecimal amount;
	public Currency currency;

	
	public Money(){}

	public Money(Fare f){
		amount = f.amount;
		currency = f.currency;
	}
}
