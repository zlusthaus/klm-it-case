package com.afkl.cases.df.upstreamdomain;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.afkl.cases.df.aop.ProvideMetricsAspect;
import com.fasterxml.jackson.databind.jsonschema.JsonSerializableSchema;

@JsonSerializableSchema
public class FullMetrics {

	public int totalNumberOfRequests;
	public int totalNumberOfRequestsOK;
	public int totalNumberOfRequests4xx;
	public int totalNumberOfRequests5xx;

	public long minResponseTime;
	public long maxResponseTime;
	
	public double averageResponseTime;

	public FullMetrics() {
	}

	public FullMetrics(Set<Entry<String, Object>> entries) {

		totalNumberOfRequests = sumAll(entries.stream(), "counter.status.");
		totalNumberOfRequestsOK = sumAll(entries.stream(), "counter.status.2");
		totalNumberOfRequests4xx = sumAll(entries.stream(), "counter.status.4");
		totalNumberOfRequests5xx = sumAll(entries.stream(), "counter.status.5");

		long totalResponseTime = getFromMeasurements(".total")
				.collect(Collectors.summingLong(e -> e));

		averageResponseTime = (double)totalResponseTime / (double)totalNumberOfRequests;
		
		maxResponseTime = getFromMeasurements(".max")
				.max(Long::compare).orElse(0L);

		minResponseTime = getFromMeasurements(".min")
				.min(Long::compare).orElse(0L);

	}

	/**
	 * adds up all the values where the key starts with the given prefix
	 */
	private int sumAll(Stream<Entry<String, Object>> stream, String metricPrefix){
		return stream.filter(e -> e.getKey().startsWith(metricPrefix)).map(Map.Entry::getValue).collect(Collectors.summingInt(e -> Integer.valueOf(e.toString())));
	}
	
	private Stream<Long> getFromMeasurements(String measurement) {
		return ProvideMetricsAspect.measurements.entrySet()
			.stream()
			.filter(e -> e.getKey().startsWith("slf4j.timer") && e.getKey().endsWith(measurement))
			.map(e->e.getValue().get());
	}

}
