package com.afkl.cases.df;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.afkl.cases.df.aop.ProvideMDCAspect;
import com.afkl.cases.df.aop.ProvideMetricsAspect;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

@Configuration
@EnableWebMvc
@EnableAutoConfiguration
@EnableAspectJAutoProxy
public class WebConfiguration extends WebMvcConfigurerAdapter {

	@Value("${klm.travelapi.url:http://localhost:8080}")
	private String baseUrl;
	@Value("${klm.travelapi.authorizeUrl:http://localhost:8080/oauth/authorize}")
	private String authorizeUrl;
	@Value("${klm.travelapi.tokenUrl:http://localhost:8080/oauth/token}")
	private String tokenUrl;

	@Value("${klm.travelapi.clientId:travel-api-client}")
	private String clientId;

	@Value("${klm.travelapi.secret:pwd}")
	private String secret;

	/**
	 * This is used for conversion instead of HttpMessageConverters simply for
	 * logging purposes. If needed,
	 */
	@Bean
	public ObjectMapper objectMapper() {
		ObjectMapper m = new ObjectMapper();
		m.configure(MapperFeature.USE_WRAPPER_NAME_AS_PROPERTY_NAME, true);
		m.setSerializationInclusion(Include.ALWAYS);
		m.enable(SerializationFeature.INDENT_OUTPUT);
		m.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		return m;
	}

	@Bean
	public OAuth2RestTemplate restTemplate() {
		ClientCredentialsResourceDetails resourceDetails = new ClientCredentialsResourceDetails();
		resourceDetails.setAccessTokenUri(tokenUrl);
		resourceDetails.setClientId(clientId);
		resourceDetails.setClientSecret(secret);
		resourceDetails.setGrantType("client_credentials");
		resourceDetails.setScope(Arrays.asList("read", "write"));

		DefaultOAuth2ClientContext clientContext = new DefaultOAuth2ClientContext();
		return new OAuth2RestTemplate(resourceDetails, clientContext);
	}
	
	@Bean
	public ProvideMetricsAspect metricsAspect(){
		return new ProvideMetricsAspect();
	}

	@Bean
	public ProvideMDCAspect mdcAspect(){
		return new ProvideMDCAspect();
	}

	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/").addResourceLocations("classpath:/static/index.html");
		registry.addResourceHandler("/index.html").addResourceLocations("classpath:/static/index.html");
		registry.addResourceHandler("/start.html").addResourceLocations("classpath:/static/start.html");
		registry.addResourceHandler("/appmetrics.html").addResourceLocations("classpath:/static/appmetrics.html");
	}

}