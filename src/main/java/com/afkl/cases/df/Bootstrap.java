package com.afkl.cases.df;

import java.io.IOException;
import java.util.concurrent.Executor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.AsyncConfigurerSupport;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableAsync
@SpringBootApplication
public class Bootstrap extends AsyncConfigurerSupport {

	public static void main(String[] args) throws IOException {
		SpringApplication.run(Bootstrap.class, args);
	}

	/**
	 * For the async executor. Shoudl be configurable.
	 */
	@Override
	public Executor getAsyncExecutor() {
		MdcThreadPoolTaskExecutor executor = new MdcThreadPoolTaskExecutor();
		executor.setCorePoolSize(20);
		executor.setMaxPoolSize(20);
		executor.setQueueCapacity(500);
		executor.setThreadNamePrefix("async-task-");
		executor.initialize();
		return executor;
	}
}
