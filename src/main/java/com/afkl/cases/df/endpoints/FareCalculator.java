
package com.afkl.cases.df.endpoints;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.afkl.cases.df.aop.ProvideMDC;
import com.afkl.cases.df.aop.ProvideMetrics;
import com.afkl.cases.df.downstreamdomain.AirportInformation;
import com.afkl.cases.df.downstreamdomain.Fare;
import com.afkl.cases.df.services.AirportFetcherService;
import com.afkl.cases.df.services.FareFetcherService;
import com.afkl.cases.df.upstreamdomain.FareInformation;
import com.afkl.cases.df.upstreamdomain.Money;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

@Controller
@RequestMapping("/fares")
public class FareCalculator {

	private final static Logger log = LoggerFactory.getLogger(FareCalculator.class);

	private final AirportFetcherService airportFetcher;
	private final FareFetcherService fareFetcher;

	@Value("${klm.travelapi.url}")
	private String travelApiUrl;

	//seconds.
	@Value("${klm.travelapi.timeout}")
	private int travelApiTimeout;

	public FareCalculator(AirportFetcherService airportFetcher, FareFetcherService fareFetcher) {
		this.airportFetcher = airportFetcher;
		this.fareFetcher = fareFetcher;
	}

	@ProvideMetrics
	@ProvideMDC
	@RequestMapping(method = RequestMethod.GET)
	public @ResponseBody FareInformation searchFare(
			@RequestParam(value = "source", required = true, defaultValue = "") String source,
			@RequestParam(value = "dest", required = true, defaultValue = "") String dest
			) throws InterruptedException, ExecutionException, TimeoutException, JsonParseException, JsonMappingException, IOException {
		
		if (log.isDebugEnabled())
			log.debug("Fare calculation between " + source + " and " + dest);
		else
			log.info("Fare calculation requested");
			
		Future<AirportInformation> sourceAirportFuture =  airportFetcher.findAirportByCode(source);
		Future<AirportInformation> destAirportFuture =  airportFetcher.findAirportByCode(dest);
		
		AirportInformation srcInfo = sourceAirportFuture.get(travelApiTimeout, TimeUnit.SECONDS);
		AirportInformation dstInfo = destAirportFuture.get(travelApiTimeout, TimeUnit.SECONDS);

		FareInformation fi = new FareInformation(srcInfo, dstInfo, null);
		
		//not worth calling the expensive service if the airport codes are incorrect
		if (fi.source != null && fi.destination != null) {
			log.debug("Source and destination found, looking for fares");
			Future<Fare> fareFuture = fareFetcher.findFares(srcInfo.code, dstInfo.code);
			Fare f = fareFuture.get(travelApiTimeout, TimeUnit.SECONDS);
			fi.cost = new Money(f);
			log.debug("Fare calculation done");
		}

		return fi;		
	}

}
