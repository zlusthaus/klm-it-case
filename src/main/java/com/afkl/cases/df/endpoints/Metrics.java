
package com.afkl.cases.df.endpoints;

import java.util.Map.Entry;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.endpoint.MetricsEndpoint;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.afkl.cases.df.aop.ProvideMDC;
import com.afkl.cases.df.upstreamdomain.FullMetrics;

/**
 * This will provide the full metrics.
 * Some of the metrics are readily available by Spring Boot, such as the number of specific requests.
 * Others are metered by using @ProvideMetrics annotation, and thus accessed from ProvideMetricsAspect
 * 
 * @author zoltanlusthaus
 *
 */
@Controller
@RequestMapping("/fullmetrics")
public class Metrics {

	private final static Logger log = LoggerFactory.getLogger(FullMetrics.class);
	
	@Autowired 
	private MetricsEndpoint me;
	
	@RequestMapping(method=RequestMethod.GET)
	@ProvideMDC
    public @ResponseBody FullMetrics getMetrics() {
		log.info("Metrics requested");
		Set<Entry<String, Object>> entries = me.invoke().entrySet();

		FullMetrics fm = new FullMetrics(entries);
		return fm;
    }
	
}
