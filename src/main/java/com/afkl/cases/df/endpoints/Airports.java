
package com.afkl.cases.df.endpoints;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.afkl.cases.df.aop.ProvideMDC;
import com.afkl.cases.df.aop.ProvideMetrics;
import com.afkl.cases.df.downstreamdomain.AirportInformation;
import com.afkl.cases.df.downstreamdomain.AirportInternalResponse;
import com.afkl.cases.df.services.AirportFetcherService;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

@Controller
@RequestMapping("/airports")
public class Airports {

	private final static Logger log = LoggerFactory.getLogger(Airports.class);

	@Autowired
	private AirportFetcherService airportFetcher;

	@ProvideMetrics
	@ProvideMDC
	@RequestMapping(method = RequestMethod.GET)
	public @ResponseBody List<AirportInformation> findAirport(@RequestParam(value = "term", required = false, defaultValue = "") String searchTerm)
			throws JsonParseException, JsonMappingException, InterruptedException, IOException, ExecutionException, TimeoutException {
		if (log.isDebugEnabled())
			log.debug(".findAirport : Trying to find airport " + searchTerm);
		else
			log.info(".findAirport initiated");
		
		if (searchTerm.length() < 3)
			return null;

		Future<AirportInternalResponse> searchedFuture = airportFetcher.findAirport(searchTerm);
		AirportInternalResponse searchedResponse = searchedFuture.get(2, TimeUnit.SECONDS);

		if (searchedResponse == null)
			return null;

		List<AirportInformation> list = searchedResponse.locations;

		log.debug("There are " + list.size() + " results.");

		list.sort((o1, o2) -> {
			return o1.code.compareTo(o2.code);
		});

		return list;
	}

}
