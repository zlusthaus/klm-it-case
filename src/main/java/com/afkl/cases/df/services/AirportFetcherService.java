package com.afkl.cases.df.services;

import java.io.IOException;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import com.afkl.cases.df.downstreamdomain.AirportInformation;
import com.afkl.cases.df.downstreamdomain.AirportInternalResponse;
import com.afkl.cases.df.downstreamdomain.AirportResponse;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class AirportFetcherService {

	private final static Logger log = LoggerFactory.getLogger(AirportFetcherService.class);

	private static final String urlTemplate = "%s/airports?term=%s";
	private static final String urlTemplateByCode = "%s/airports/%s";

	@Value("${klm.travelapi.url}")
	private String travelApiUrl;

	@Autowired
	private OAuth2RestTemplate restTemplate;

	@Autowired
	private ObjectMapper mapper;

	public AirportFetcherService() {

	}

	@Async
	public Future<AirportInternalResponse> findAirport(String searchTerm)
			throws InterruptedException, JsonParseException, JsonMappingException, IOException {
		
		log.debug("Looking for airport based on term '" + searchTerm + "'");

		String actualUrl = String.format(urlTemplate, travelApiUrl, searchTerm);

		if (log.isTraceEnabled())
			log.trace("URL to be called : " + actualUrl);
		
		try {
			String info = restTemplate.getForObject(actualUrl, String.class);
			log.trace(info);
			AirportResponse ar = mapper.readValue(info.toString(), AirportResponse.class);
			return new AsyncResult<>(ar._embedded);
		} catch (HttpClientErrorException he) {
			if (he.getRawStatusCode() != 404)
				log.warn("Error while looking for an airport", he);
			return new AsyncResult<>(null);
		}

	}

	@Async
	public Future<AirportInformation> findAirportByCode(String code)
			throws InterruptedException, JsonParseException, JsonMappingException, IOException {
		log.debug("Looking for airport by code '" + code + "'");
		String actualUrl = String.format(urlTemplateByCode, travelApiUrl, code);

		if (log.isTraceEnabled())
			log.trace("URL to be called : " + actualUrl);

		try {
			String info = restTemplate.getForObject(actualUrl, String.class);
			log.trace(info);

			AirportInformation ar = mapper.readValue(info.toString(), AirportInformation.class);
			return new AsyncResult<>(ar);
		} catch (HttpClientErrorException he) {
			if (he.getRawStatusCode() != 404)
				log.warn("Error while looking for an airport", he);
			return new AsyncResult<>(null);
		}

	}

}
