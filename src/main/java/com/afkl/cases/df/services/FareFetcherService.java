package com.afkl.cases.df.services;

import java.io.IOException;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.stereotype.Service;

import com.afkl.cases.df.downstreamdomain.Fare;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class FareFetcherService {

	private static final String urlTemplate = "%s/fares/%s/%s";

	private final static Logger log = LoggerFactory.getLogger(FareFetcherService.class);

	@Value("${klm.travelapi.url}")
	private String travelApiUrl;

	@Autowired
	private OAuth2RestTemplate restTemplate;

	@Autowired
	private ObjectMapper mapper;

	public FareFetcherService() {

	}

	@Async
	public Future<Fare> findFares(String originCode, String destinationCode)
			throws InterruptedException, JsonParseException, JsonMappingException, IOException {
		log.info("Looking for fares from " + originCode + " to " + destinationCode);
		String actualUrl = String.format(urlTemplate, travelApiUrl, originCode, destinationCode);

		log.debug("URL to call is " + actualUrl);
		String fare = restTemplate.getForObject(actualUrl, String.class);
		log.trace(fare);
		
		Fare fObject = mapper.readValue(fare.toString(), Fare.class);

		return new AsyncResult<>(fObject);
	}

}
